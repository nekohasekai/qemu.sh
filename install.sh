info() { echo "I: $@"; }; error() { echo "E: $@";exit 1; }

install_dir="$HOME/.qemu.sh"; [ $1 ] && install_dir="$1"

if [ -x "$(command -v git)" ]; then
  
  info ">> Install"
  
  git clone https://gitlab.com/KazamaWataru/qemu.sh.git $install_dir
  
  alias=$(echo alias qemu="$install_dir/qemu.sh")

  echo -e "\n$alias" >> ~/.bashrc

  $alias

  info "<< Install"
  
  exit 0

elif [ -x "$(command -v wget)" ]; then

  download() { wget "$1" -O "$2"; }
  
elif [ -x "$(command -v curl)" ]; then

  download() { curl "$1" -o "$2"; }
  
else

  error "git or curl or wget required."

fi

if [ -x "$(command -v tar)" ]; then

  if [ -x "$(command -v bzip2)" ]; then
  
    target="tar.bz2"
  
  elif [ -x "$(command -v gzip)" ]; then

    target="tar.gz"
  
  else

    target="tar"
  
  fi
  
  unpack() { tar -xvf "$1"; rm -rf "$1"; }

elif [ -x "$(command -v unzip)" ]; then

  target="zip"

  unpack() { unzip "$1"; rm -rf "$1"; }
  
else

  error "tar or unzip required."

fi

rm -rf "$install_dir"

info ">> Install"

download "https://gitlab.com/KazamaWataru/qemu.sh/-/archive/master/qemu.sh-master.$target" "qemu.sh.$target" || exit 1

unpack "qemu.sh.$target"

mv "qemu.sh-master" "$install_dir"

alias=$(echo alias qemu="$install_dir/qemu.sh")

echo -e "\n$alias" >> ~/.bashrc

$alias

info "<< Install"