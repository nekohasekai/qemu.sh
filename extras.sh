qemu install_all

# init

qemu exec_all apt install -y locales dialog apt-utils

qemu exec_all localedef -f UTF-8 -i zh_CN zh_CN.UTF-8

# arch

qemu exec_all uname -m

# td

qemu execs_all <<EOF

  apt install -y gperf git cmake g++ zlib1g-dev libssl-dev openjdk-8-jdk

  export JAVA_HOME="/usr/lib/jvm/$(ls /usr/lib/jvm | head -n 1)"
  
  # git clone https://gitlab.com/tooko/tooko-tdlib /opt/td --single-branch

  cd /opt/td
  
  [ -f "build/libtdjni.so" ] && exit 0

  git submodule init
  git submodule update
  
  cd td
  
  git fetch
  
  git checkout FETCH_HEAD
  
  rm -rf build && mkdir build && cd build
  
  cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX:PATH=../../td -DTD_ENABLE_JNI=ON ..
  
  cmake --build . --target install
  
  cd ..
  
  rm -rf build && mkdir build && cd build
  
  cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX:PATH=../ -DTd_DIR:PATH=$(readlink -e ../td/lib/cmake/Td) ..

  cmake --build . --target install
  
EOF

# webp

DISTS=(amd64 i386 arm64 armhf armel mipsel mips64el ppc64el s390x)

qemu execs_all <<EOF
  
  # apt install -y git cmake g++ zlib1g-dev openjdk-8-jdk libpng-dev libtiff-dev libjpeg-dev libgif-dev freeglut3-dev libsdl1.2-dev

  # git clone https://gitlab.com/tooko/tooko-webp /opt/webp

  cd /opt/webp
  
  [ -f "libwebp-imageio.so" ] && exit 0
  
  # git submodule init
  # git submodule update
 
  export JAVA_HOME="/usr/lib/jvm/\$(ls /usr/lib/jvm | head -n 1)"
  
  ./jni.sh

EOF

rm -rf dists

for arch in ${DISTS[@]}; do

  mkdir -p dists/$arch.

  cp rootfs/$arch/opt/webp/libwebp-imageio.so dists/$arch

done