ROOTFS_DIR="/root/rootfs"

#工具链

apt -y install \
  make git gperf php-fpm cmake default-jdk \
  g++-arm-linux-gnueabi \
  g++-arm-linux-gnueabihf \
  g++-aarch64-linux-gnu \
  g++-i686-linux-gnu \
  g++-x86-64-linux-gnu \
  g++-mips64el-linux-gnuabi64 \
  g++-mipsel-linux-gnu \
  g++-powerpc64le-linux-gnu \
  g++-s390x-linux-gnu
  
# zlib 的软连接 chroot 外无法读取

qemu exec_all apt install -y zlib1g-dev libssl-dev openjdk-8-jdk

qemu execs_all <<EOF

  cd /usr/lib/*-linux-*/
  mv libz.so libz.so.bak
  mv libz.so.bak libz.so
  
EOF

cd /opt

rm -rf td

git clone https://gitlab.com/tooko/tooko-tdlib td --single-branch

cd td

git submodule init
git submodule update

cd td

git fetch
  
git checkout FETCH_HEAD

mkdir native_build && cd native_build

cmake ..

cmake --build . --target prepare_cross_compiling

cd ..

ROOTFS_DIR="/root/rootfs"

cd /opt/td/td

function build_tdlib() {

  local processer="$1"
  local arch="$2"
  local tool="$3"

  rm -rf "build-$arch" && mkdir -p "build-$arch" && cd "build-$arch"

  export CXXFLAGS=""
  
  JAVA_HOME="$ROOTFS_DIR/$arch/usr/lib/jvm"

  JAVA_HOME="${JAVA_HOME}/$(ls $JAVA_HOME | head -n 1)"

  cmake \
    -DCMAKE_SYSTEM_NAME=Linux \
    -DCMAKE_SYSTEM_PROCESSOR="$processer" \
    -DCMAKE_C_COMPILER="$tool-gcc" \
    -DCMAKE_CXX_COMPILER="$tool-g++" \
    -DCMAKE_SYSROOT="$ROOTFS_DIR/$arch" \
    -DJAVA_AWT_LIBRARY="$JAVA_HOME/lib/libjawt.so" \
    -DJAVA_JVM_LIBRARY="$JAVA_HOME/lib/server/libjvm.so" \
    -DJAVA_INCLUDE_PATH="$JAVA_HOME/include" \
    -DJAVA_INCLUDE_PATH2="$JAVA_HOME/include/linux" \
    -DJAVA_AWT_INCLUDE_PATH="$JAVA_HOME/include" \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX:PATH=../../td \
    -DCMAKE_FIND_ROOT_PATH_MODE_PROGRAM=NEVER \
    -DCMAKE_FIND_ROOT_PATH_MODE_PACKAGE=ONLY \
    -DTD_ENABLE_JNI=ON ..

  cmake --build . --target install

  cd ../..

  mkdir "build-$arch" && cd "build-$arch"

  cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX:PATH=../ -DTd_DIR:PATH=$(readlink -e ../td/lib/cmake/Td) ..

  cmake --build . --target install
  
}

build_tdlib x86_64 amd64 x86_64-linux-gnu
build_tdlib i686 i386 i686-linux-gnu
build_tdlib arm armel arm-linux-gnueabi
build_tdlib arm armhf arm-linux-gnueabihf
build_tdlib arm arm64 aarch64-linux-gnu
build_tdlib mipsel mipsel mipsel-linux-gnu
build_tdlib mips64el mips64el mips64el-linux-gnuabi64
build_tdlib powerpc64le ppc64el powerpc64le-linux-gnu
build_tdlib s390x s390x g++-s390x-linux-gnu

