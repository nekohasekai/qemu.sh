#!/bin/bash

ROOTFS_DIR="/root/rootfs"

msg() {

  echo "$@"
  
}

start() {

  msg ">> " $@

}

end() {

  msg "<< " $@

}

error() {

  msg "E: " $@
  
  exit 1

}

is_ok() {

  if [ $? -eq 0 ]; then
  
    if [ -n "$2" ]; then
    
      msg "$2"
      
    fi
    
    return 0
    
  else
  
    if [ -n "$1" ]; then
    
      msg "$1"
      
    fi
    
    return 1
    
  fi
  
}

get_platform() {

  local arch="$1"
  
  if [ -z "$arch" ]; then
  
    arch=$(uname -m)
    
  fi
  
  case "$arch" in
  
    arm64|aarch64) echo "arm64" ;;
  
    arm*)
  
      local hard_float=$(readelf -a /usr/bin/readelf | grep armhf)
    
      if [ -z "$hard_float" ]; then
    
        echo "armhf"
      
      else
    
        echo "armel"
      
      fi
      
    ;;
  
    x86_64|amd64) echo "amd64" ;;
  
    i[3-6]86|x86) echo "i386" ;;
  
    *) echo "unknown" ;;
  
  esac
  
}

is_mounted() {

  local mount_point="$1"
  
  [ -n "$mount_point" ] || return 1
  
  if $(grep -q " ${mount_point%/} " /proc/mounts); then
  
    return 0
    
  else
  
    return 1
    
  fi
}

get_pids() {
 
  local arch="$1"
  local rootfs_dir="$ROOTFS_DIR/$arch"
  local pid pidfile pids
  
  shift
  
  for pid in $@; do
  
    pidfile="$rootfs_dir$pid"
    
    if [ -e "$pidfile" ]; then
    
      pid=$(cat "$pidfile")
      
    fi
    
    if [ -e "/proc/$pid" ]; then
    
      pids="$pids $pid"
      
    fi
    
  done
  
  if [ -n "$pids" ]; then
  
    echo $pids
    
    return 0
    
  else
  
    return 1
    
  fi
  
}

is_started() {
 
  get_pids arch $@ > /dev/null
  
}

is_stopped() {

  is_started $*
  
  test $? -ne 0
  
}

kill_pids() {

  local pids=$(get_pids $@)
  
  if [ -n "$pids" ]; then
  
    kill -9 $pids
    
    return $?
    
  fi
  
  return 0
  
}

qemu_mount() {
  
  for arch in $@; do
  
    start "Mounting... $arch"
  
    local item
  
    for item in proc sys dev shm pts fd tty tun; do
  
      mount_part $arch $item || return 1
    
    done
 
  done

  return 0
  
}

mount_part() {
  
  local arch="$1"; shift
  local rootfs_dir="$ROOTFS_DIR/$arch"
  
  case "$1" in
  
    proc)
    
      msg -n "/proc ... "
      
      local target="${rootfs_dir}/proc"
      
      if ! is_mounted "${target}" ; then
      
        [ -d "${target}" ] || mkdir -p "${target}"
        mount -t proc proc "${target}"
        
        is_ok "fail" "done"
        
      else
      
        msg "skip"
        
      fi
      
    ;;
    
    sys)
    
      msg -n "/sys ... "
      
      local target="${rootfs_dir}/sys"
      
      if ! is_mounted "${target}" ; then
      
        [ -d "${target}" ] || mkdir -p "${target}"
        mount -t sysfs sys "${target}"
        
        is_ok "fail" "done"
        
      else
      
        msg "skip"
        
      fi
    ;;
    
    dev)
    
      msg -n "/dev ... "
      
      local target="${rootfs_dir}/dev"
      
      if ! is_mounted "${target}" ; then
        
        [ -d "${target}" ] || mkdir -p "${target}"
        mount -o bind /dev "${target}"
        
        is_ok "fail" "done"
        
      else
      
        msg "skip"
        
      fi
      
    ;;
    
    shm)
    
      msg -n "/dev/shm ... "
      
      if ! is_mounted "/dev/shm" ; then
      
        [ -d "/dev/shm" ] || mkdir -p /dev/shm
        mount -o rw,nosuid,nodev,mode=1777 -t tmpfs tmpfs /dev/shm
        
      fi
      
      local target="${rootfs_dir}/dev/shm"
      
      if ! is_mounted "${target}" ; then
      
        mount -o bind /dev/shm "${target}"
        
        is_ok "fail" "done"
        
      else
      
        msg "skip"
        
      fi
      
    ;;
    
    pts)
    
      msg -n "/dev/pts ... "
      
      if ! is_mounted "/dev/pts" ; then
      
          [ -d "/dev/pts" ] || mkdir -p /dev/pts
          
          mount -o rw,nosuid,noexec,gid=5,mode=620,ptmxmode=000 -t devpts devpts /dev/pts
          
      fi
      
      local target="${rootfs_dir}/dev/pts"
      
      if ! is_mounted "${target}" ; then
      
          mount -o bind /dev/pts "${target}"
          
          is_ok "fail" "done"
          
      else
      
          msg "skip"
          
      fi
      
    ;;
    
    fd)
    
      if [ ! -e "/dev/fd" -o ! -e "/dev/stdin" -o ! -e "/dev/stdout" -o ! -e "/dev/stderr" ]; then
      
        msg -n "/dev/fd ... "
        
        [ -e "/dev/fd" ] || ln -s /proc/self/fd /dev/
        [ -e "/dev/stdin" ] || ln -s /proc/self/fd/0 /dev/stdin
        [ -e "/dev/stdout" ] || ln -s /proc/self/fd/1 /dev/stdout
        [ -e "/dev/stderr" ] || ln -s /proc/self/fd/2 /dev/stderr
        
        is_ok "fail" "done"
        
      fi
      
    ;;
    
    tty)
    
      if [ ! -e "/dev/tty0" ]; then
      
        msg -n "/dev/tty ... "
        
        ln -s /dev/null /dev/tty0
        
        is_ok "fail" "done"
        
      fi
      
    ;;
    
    tun)
    
      if [ ! -e "/dev/net/tun" ]; then
      
        msg -n "/dev/net/tun ... "
        
        [ -d "/dev/net" ] || mkdir -p /dev/net
        mknod /dev/net/tun c 10 200
            
        is_ok "fail" "done"
        
      fi
      
    ;;
    
    binfmt_misc)
    
      local binfmt_dir="/proc/sys/fs/binfmt_misc"
      
      if ! is_mounted "${binfmt_dir}" ; then
        
        msg -n "${binfmt_dir} ... "
        
        mount -t binfmt_misc binfmt_misc "${binfmt_dir}"
        
        is_ok "fail" "done"
        
      fi
      
    ;;
    
    esac

    return 0
    
}

qemu_mounted() {

  local arch="$1"; shift 1
  local rootfs_dir="$ROOTFS_DIR/$arch"
  
  local mask
  
  for mask in '.*' '*'; do
  
    local parts=$(cat /proc/mounts | awk '{print $2}' | grep "^${rootfs_dir%/}/${mask}$" | sort -r)
  
    [ -z "$parts" ] || return 0
  
  done
  
  return 1
  
}

qemu_umount() {
  
  for arch in $@; do
  
    local rootfs_dir="$ROOTFS_DIR/$arch"
  
    start "Umounting $arch ... "
  
    local is_release=0
  
    local lsof_full=$(lsof | awk '{print $1}' | grep -c '^lsof')
  
    if [ "${lsof_full}" -eq 0 ]; then
  
     local pids=$(lsof | grep "${rootfs_dir%/}" | awk '{print $1}' | uniq)
     
    else
  
      local pids=$(lsof | grep "${rootfs_dir%/}" | awk '{print $2}' | uniq)
      
    fi
  
    kill_pids $arch ${pids}; is_ok "fail" "done"

    local is_mnt=0
    local mask
  
    for mask in '.*' '*'; do
  
      local parts=$(cat /proc/mounts | awk '{print $2}' | grep "^${rootfs_dir%/}/${mask}$" | sort -r)
      local part
    
      for part in ${parts}; do
    
        local part_name=$(echo ${part} | sed "s|^${rootfs_dir%/}/*|/|g")
     
        msg -n "${part_name} ... "
      
        for i in 1 2 3; do
      
          umount ${part} 2> /dev/null && break
        
        done
      
        is_ok "fail" "done"
        is_mnt=1
      
      done
      
    done
  
    [ "${is_mnt}" -eq 1 ]; is_ok " ...nothing mounted"

    local loop=$(losetup -a | grep "${rootfs_dir%/}" | awk -F: '{print $1}')
  
    if [ -n "${loop}" ]; then
  
      msg -n "Disassociating loop device ... "
      losetup -d "${loop}"
      is_ok "fail" "done"
    
    fi
    
  done

  return 0
  
}

qemu_install() {
  
  [ -d "rootfs" ] || mkdir rootfs
  
  start "Install"
  
  for arch in $@; do
    
    start "Install $arch ..."
    
    local rootfs_dir="$ROOTFS_DIR/$arch"
  
    if [ -d "$rootfs_dir" ]; then
    
      msg "already installed.."
      
      continue
    
    fi
  
    qemu-debootstrap --arch=$arch \
      --keyring /usr/share/keyrings/debian-archive-keyring.gpg --variant=buildd --exclude=debfoster \
      sid "$rootfs_dir" http://ftp.debian.org/debian
  
  done
  
  end "Install"

}

qemu_uninstall() {

  for arch in $@; do
  
    start "Uninstall $arch"
  
    qemu_mounted $arch && qemu_umount $arch
  
    local rootfs_dir="$ROOTFS_DIR/$arch"
    
    rm -rf $rootfs_dir
  
  done
  
  end "Uninstall"

}

qemu_exec() {
  
  local arch="$1"; shift 1
  local rootfs_dir="$ROOTFS_DIR/$arch"
  
  start "Exec in $arch"
  
  qemu_mounted $arch || qemu_mount $arch || return 1

  unset TMP TEMP TMPDIR LD_PRELOAD LD_DEBUG
  local path="${PATH}:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
  
  PATH="$path" chroot "$rootfs_dir" $@
  
}

DISTS=(amd64 i386 arm64 armhf armel mipsel mips64el ppc64el s390x)

CMD="$1"; shift

case "$CMD" in

init) 

  apt-get install qemu qemu-user-static binfmt-support debootstrap

;;

mount)

  qemu_mount $@
  
;;

mount_all)

  qemu_mount ${DISTS[@]}
  
;;

umount)

  qemu_umount $@
  
;;

umount_all)

  qemu_umount ${DISTS[@]}
  
;;

install)

  qemu_install $@ 
  
;;

install_all)

  qemu_install ${DISTS[@]}
  
;;

uninstall)

  qemu_uninstall $@

;;

uninstall_all)

  qemu_uninstall ${DISTS[@]}

;;

reinstall)

  qemu_uninstall $@
  qemu_install $@
  
;;

reinstall_all)

  qemu_uninstall ${DISTS[@]}
  qemu_install ${DISTS[@]}
  
;;

exec)
  
  qemu_exec $@
  
;;

exec_all)
  
  for arch in ${DISTS[@]}; do
  
    qemu_exec $arch $@
  
  done
  
;;

execs_all)
  
  while read data; do cmd="$cmd\n$data"; done
  
  for arch in ${DISTS[@]}; do
  
    echo -e $cmd | qemu_exec $arch
  
  done
  
;;

esac